var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var ForumSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    created_at: Date,
    sub_title_1: {
        type: String
    },
    sub_title_1_content: {
        type: String
    },
    created_by: {
        type: String
    },
    comments: [
        {
            posted_on: Date,
            author: {
                type: String
            },
            comment_text: {
                type: String
            },
        },
    ]
});

// always be executed before the object is saved to the database
ForumSchema.pre('save', function (next) {
    var forum = this;
    // get the current date
    var currentDate = new Date();
 
    // if created_at doesn't exist, add to that field
    if (!forum.created_at) {
        forum.created_at = currentDate;
    }
    next();
});
 
module.exports = mongoose.model('forumTbl', ForumSchema);