var express = require('express');

var app = module.exports = express.Router();

var forumTbl = require('./forumTbl');

// GET
// Get all open
app.get("/forum", function (req, res) {
    forumTbl.find().sort({"created_at": -1}).exec(function (err, forums) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while get Forum", "error": err });
        }
        res.status(200).send({ "success": true, "result": forums });
    });
});

app.get("/forum/:id", function (req, res) {
    var forumId = req.params.id;
    if (!forumId || forumId === "") {
        return res.json({ "success": false, "msg": "You need to send the ID of the Forum", "error": err });
    }
    forumTbl.find({ _id: forumId }, function (err, forum) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while get Forum", "error": err });
        }
        res.status(200).send({ "success": true, "result": forum });
    });
});

// POST
// Create a new
app.post("/forum", function (req, res) {
    if (!req.body.title) {
        return res.status(400).send({ "success": false, "msg": "You need to send the title of the forum!" });
    }

    var newForum = new forumTbl({
        title: req.body.title,
        content: req.body.content,
        sub_title_1: req.body.sub_title_1,
        sub_title_1_content: req.body.sub_title_1_content,
        created_by: req.body.created_by,
    });

    newForum.save(function (err) {
        if (err) {
            console.log("Error while post: ", err);
            return res.json({ "success": false, "msg": "Error while creating", "error": err });
        }
        res.status(201).send({ "success": true, "msg": "Successfully created..!" })
    });
});

// PUT
// update
app.put("/forum/:id", function (req, res) {
    var forumId = req.params.id;

    forumTbl.update({ _id: forumId }, req.body, function (err) {
        if (err) {
            console.log("Error while post: ", err);
            return res.json({ "success": false, "msg": "Error while updating", "error": err });
        }
        res.status(201).send({ "success": true, "msg": "Successfully updated..!" })
    });
});

// add comments and modify
app.put("/forum/comment/:id", function (req, res) {
    var forumId = req.params.id;
    var currentDate_ = new Date();
    var author_ = req.body.author;
    var comment_text_ = req.body.comment_text;

    forumTbl.findOne({
        _id: forumId,
        "comments._id": req.body._id
    }, function (err, comnt) {
        // hanlde err..
        if (comnt) {
            // comnt exists 
            console.log('exists');

            forumTbl.update(
                { _id: forumId, "comments._id": req.body._id },
                {
                    $set: {
                        "comments.$.posted_on": req.body.posted_on,
                        "comments.$.author": author_,
                        "comments.$.comment_text": comment_text_
                    }
                },
                function (err) {
                    if (err) {
                        console.log("Error while post: ", err);
                        return res.json({ "success": false, "msg": "Error while updating comment", "error": err });
                    }
                    console.log(req.body._id, ' ', req.body.author, ' ', req.body.comment_text);
                    res.status(201).send({ "success": true, "msg": "Successfully Updated comment..!" })
                });


        } else {
            // comnt does not exist
            console.log('not exist');
            forumTbl.update(
                { _id: forumId },
                {
                    $push: {
                        comments: {
                            posted_on: currentDate_,
                            author: author_,
                            comment_text: comment_text_
                        }
                    }
                },
                function (err) {
                    if (err) {
                        console.log("Error while post: ", err);
                        return res.json({ "success": false, "msg": "Error while Adding comment", "error": err });
                    }
                    console.log(req.body._id, ' ', req.body.author, ' ', req.body.comment_text);
                    res.status(201).send({ "success": true, "msg": "Successfully comment Added..!" })
                });
        }
    })
});

// UPDATE
// Remove comment
app.put("/forum/comment/:fid/:cid", function (req, res) {
    var forumId = req.params.fid;

    forumTbl.findOneAndUpdate(
        { _id: req.params.fid },
        {
            $pull: {
                comments: { _id: req.params.cid }
            }
        },
        function (err) {
            if (err) {
                console.log("Error while post: ", err);
                return res.json({ "success": false, "msg": "Error while deleting comment", "error": err });
            }
            console.log(req.params.fid, ' ', req.params.cid);
            res.status(201).send({ "success": true, "msg": "Successfully Comment Deleted..!" })
        });
});

// DELETE
// Remove one by its ID
app.delete("/forum/:id", function (req, res) {
    var forumId = req.params.id;
    if (!forumId || forumId === "") {
        return res.json({ "success": false, "msg": "You need to send the ID of the Forum", "error": err });
    }

    forumTbl.findByIdAndRemove(forumId, function (err, removed) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while deleting Forum", "error": err });
        }
        res.status(200).json({ "success": true, "msg": "Forum deleted" });
    });
});