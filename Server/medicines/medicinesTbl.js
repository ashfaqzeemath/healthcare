var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var MedicineSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    created_by: {
        type: String,
        required: true
    }
});

// always be executed before the object is saved to the database
MedicineSchema.pre('save', function (next) {
    var medicine = this;
    // get the current date
    var currentDate = new Date();
 
    // if created_at doesn't exist, add to that field
    if (!medicine.created_at) {
        medicine.created_at = currentDate;
    }
    next();
});
 
module.exports = mongoose.model('medicineTbl', MedicineSchema);