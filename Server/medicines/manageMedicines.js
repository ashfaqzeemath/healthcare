var express = require('express');

var app = module.exports = express.Router();

var medicineTbl = require('./medicinesTbl');

// GET
// Get all open
app.get("/medicines", function (req, res) {
    medicineTbl.find().sort({"name": 1}).exec(function (err, medicine) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while get medicines", "error": err });
        }
        res.status(200).send({ "success": true, "result": medicine });
    });
});

app.get("/medicine/:id", function (req, res) {
    var medicineId = req.params.id;
    if (!medicineId || medicineId === "") {
        return res.json({ "success": false, "msg": "You need to send the ID of the medicine", "error": err });
    }
    medicineTbl.find({_id: medicineId}, function (err, medicine) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while get medicine", "error": err });
        }
        res.status(200).send({ "success": true, "result": medicine });
    });
});

// POST
// Create a new
app.post("/medicine", function (req, res) {
    if (!req.body.name) {
        return res.status(400).send({ "success": false, "msg": "You need to send the name of the medicine!" });
    }

    var newMedicine = new medicineTbl({
        name: req.body.name,
        description: req.body.description,
        created_by: req.body.created_by
    });

    newMedicine.save(function (err) {
        if (err) {
            console.log("Error while post: ", err);
            return res.json({ "success": false, "msg": "Error while creating", "error": err });
        }
        res.status(201).send({ "success": true, "msg": "Successfully created..!" })
    });
});

// PUT
// update
app.put("/medicine/:id", function (req, res) {
    var medicineId = req.params.id;
    
    medicineTbl.update({_id: medicineId}, req.body, function (err) {
        if (err) {
            console.log("Error while post: ", err);
            return res.json({ "success": false, "msg": "Error while updating", "error": err });
        }
        res.status(201).send({ "success": true, "msg": "Successfully updated..!" })
    });
});

// DELETE
// Remove one by its ID
app.delete("/medicine/:id", function (req, res) {
    var medicineId = req.params.id;
    if (!medicineId || medicineId === "") {
        return res.json({ "success": false, "msg": "You need to send the ID of the Medicine", "error": err });
    }

    medicineTbl.findByIdAndRemove(medicineId, function (err, removed) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while deleting Medicine", "error": err });
        }
        res.status(200).json({ "success": true, "msg": "Medicine deleted" });
    });
});