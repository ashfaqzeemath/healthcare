const express= require('express');
var app = module.exports = express.Router();
const worktbl = require('./worktbl');


app.post('/saveworkoutchart',function(req,res)
{     
    const newworktbl = new worktbl({ 
            bms: req.body.bms,
            day: req.body.day,
            pushups: req.body.pushups,
            squats: req.body.squats,
            situps: req.body.situps,
            lunges: req.body.lunges
   }); 

         worktbl.seveworkoutchart(newworktbl, function(err,worktbl){     
             if(err){
                    res.json({status:false,msg:"data not inserted"});
             }
             if(worktbl){
                 console.log(newworktbl); 
                    res.json({status:true,msg:"data inserted"});
             }
         });

});   


app.post('/bms2',function(req, res) {
     
const bms = req.body.bms;

worktbl.findbybms2(bms,function(err, bms){
            if(err) throw err;

            if(bms){
                    console.log(bms);
            }
    });
  }
 );



  app.delete("/workoutchart/:id", function (req, res) {
    var workoutchartid = req.params.id;
    if (!workoutchartid || workoutchartid === "") {
        return res.json({ "success": false, "msg": "You need to send the ID of the Workout chart", "error": err });
    }

    worktbl.findByIdAndRemove(workoutchartid, function (err, removed) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while deleting Workout chart", "error": err });
        }
        res.status(200).json({ "success": true, "msg": "Workout chart deleted" });
    });
});



 app.put("/workoutchart/:id", function (req, res) {
    var workoutchartid = req.params.id;
    
    worktbl.update({_id: workoutchartid}, req.body, function (err) {
        if (err) {
            console.log("Error while post: ", err);
            return res.json({ "success": false, "msg": "Error while updating", "error": err });
        }
        res.status(302).send({ "success": true, "msg": "Successfully updated..!" })
    });
});