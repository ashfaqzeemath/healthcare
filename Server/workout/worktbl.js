var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var WorkoutSchema = new Schema({
    
    bms: { type: Number,required: true },
    day: { type: String,required: true},
    pushups: { type: String,required: true},
    squats: { type: String,required: true},
    situps: { type: String,required: true},
    lunges:   { type:String,required: true}

});

const WorkoutTble=  module.exports= mongoose.model('WorkoutTble',WorkoutSchema);

module.exports.seveworkoutchart= function(WorkoutSchema,callback){
         WorkoutSchema.save(callback);
};



module.exports.findbybms2= function(bms,callback){
    const query ={bms:bms};
    WorkoutTble.findOne(query,callback);

};
