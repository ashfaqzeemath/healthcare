const express= require('express');
var app = module.exports = express.Router();
const dietchartTbl = require('./dietchartTbl');


app.post('/savedietchart',function(req,res)
{     
    const newdietchart = new dietchartTbl({ 
            bms: req.body.bms,
            morning1: req.body.morningmeal1,
            morning2: req.body.morningmeal2,
            morning3: req.body.morningmeal3,
            morning4: req.body.morningmeal4,
            lunch1  : req.body.lunchmeal1,
            lunch2  : req.body.lunchmeal2,
            lunch3  : req.body.lunchmeal3,
            lunch4  : req.body.lunchmeal4,
            dinner1 : req.body.dinnermeal1,
            dinner2 : req.body.dinnermeal2,
            dinner3 : req.body.dinnermeal3,
            dinner4 : req.body.dinnermeal4,
   }); 

     console.log(newdietchart);
   console.log(req.body.dinner4);
         dietchartTbl.sevedietchart(newdietchart, function(err,diet){     
             if(err){
                    res.json({status:false,msg:"data not inserted"});
             }
             if(diet){
                 console.log(newdietchart); 
                    res.json({status:true,msg:"data inserted"});
             }
         });
});   



app.post('/bms',function(req, res) {
     
const bms = req.body.bms;

dietchartTbl.findbybms(bms,function(err, bms){
            if(err) throw err;

            if(bms){
                    console.log(bms);
            }
    });
  }
 );



app.delete("/dietchat/:bms", function (req, res) {
    var dietchartid =""; 
    var bms = req.params.bms;
    console.log(bms);
    dietchartTbl.findOne({bms:bms},function(err, bms){
            if(err) 
            console.log(err);

            if(bms){
                 dietchartid =   bms._id
                 console.log(dietchartid);
                  if ( dietchartid === "") {
    return res.json({ "success": false, "msg": "You need to send the ID of the diet plan" });
    }

    dietchartTbl.findByIdAndRemove(dietchartid, function (err, removed) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while deleting diet plan", "error": err });
        }
        res.status(200).json({ "success": true, "msg": "Forum deleted" });
    });
            }

             });

   
});



app.get("/checkbms/:bms",function(req, res) {
     var bms = req.params.bms;
dietchartTbl.findOne({bms:bms},function(err, bms){
            if(err) 
            console.log(err);;

            res.status(200).send({"success": true, "result": bms});
    });
  }
 );

app.get("/assigndietchart/:bms",function(req, res) {
     var bms = req.params.bms;
dietchartTbl.findOne({bms:bms},function(err, bms){
            if(err) 
            console.log(err);;

            res.status(200).send({"success": true, "result": bms});
    });
  }
 );

app.get("/searchdietchart",function(req, res) {
     
dietchartTbl.find({},function(err, bms){
            if(err) throw err;

            res.status(200).send({"success": true, "result": bms});
    });
  }
 );


app.put("/updetdietchat/:bms", function (req, res) {

    var dietchartid =""; 

    var bms = req.params.bms;

    console.log(bms);
    dietchartTbl.findOne({bms:bms},function(err, bms){
            if(err) 
            console.log(err);

             if(bms){
                 dietchartid =   bms._id
                 console.log(dietchartid);
                  if ( dietchartid === "") 
                  {
          return res.json({ "success": false, "msg": "You need to send the ID of the diet plan" });
                }

          dietchartTbl.update({_id: dietchartid}, req.body, function (err) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while updating diet plan", "error": err });
        }
        res.status(200).json({ "success": true, "msg": " diet plan updated"  });
    });
            }

             });

   
});
