var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var FeedbackSchema = new Schema({
    subject: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    created_by: {
        type: String,
        required: true
    },
    created_at: Date
});

// always be executed before the object is saved to the database
FeedbackSchema.pre('save', function (next) {
    var feedback = this;
    // get the current date
    var currentDate = new Date();
 
    // if created_at doesn't exist, add to that field
    if (!feedback.created_at) {
        feedback.created_at = currentDate;
    }
    next();
});
 
module.exports = mongoose.model('feedbackTbl', FeedbackSchema);