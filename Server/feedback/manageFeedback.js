var express = require('express');

var app = module.exports = express.Router();

var feedbackTbl = require('./feedbackTbl');

// GET
// Get all open
app.get("/feedbacks", function (req, res) {
    feedbackTbl.find().sort({"created_at": -1}).exec(function (err, feedbacks) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while get feedbacks", "error": err });
        }
        res.status(200).send({ "success": true, "result": feedbacks });
    });
});

// POST
// Create a new
app.post("/feedback", function (req, res) {
    if (!req.body.name) {
        return res.status(400).send({ "success": false, "msg": "You need to send the name of the feedback!" });
    }

    var newFeedback = new feedbackTbl({
        subject: req.body.subject,
        description: req.body.description,
        created_by: req.body.created_by
    });

    newFeedback.save(function (err) {
        if (err) {
            console.log("Error while post: ", err);
            return res.json({ "success": false, "msg": "Error while creating", "error": err });
        }
        res.status(201).send({ "success": true, "msg": "Successfully Added..!" })
    });
});

// PUT
// update
app.put("/feedback/:id", function (req, res) {
    var feedbackId = req.params.id;
    
    feedbackTbl.update({_id: feedbackId}, req.body, function (err) {
        if (err) {
            console.log("Error while post: ", err);
            return res.json({ "success": false, "msg": "Error while updating", "error": err });
        }
        res.status(201).send({ "success": true, "msg": "Successfully updated..!" })
    });
});

// DELETE
// Remove one by its ID
app.delete("/feedback/:id", function (req, res) {
    var feedbackId = req.params.id;
    if (!feedbackId || feedbackId === "") {
        return res.json({ "success": false, "msg": "You need to send the ID of the Feedback", "error": err });
    }

    feedbackTbl.findByIdAndRemove(feedbackId, function (err, removed) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while deleting Feedback", "error": err });
        }
        res.status(200).json({ "success": true, "msg": "Feedback deleted" });
    });
});