const mongoose = require('mongoose');
const schema =mongoose.Schema;
var bcrypt = require('bcrypt');
const saltRounds = 10;



const userSchema = new schema({

username:{type:String,required:true},
role:{type:Number,required:true},
email:{type:String,required:true},
password:{type:String,required:true} 

});


const User =  module.exports= mongoose.model('User',userSchema);



module.exports.seveUser= function(newUser,callback){

bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(newUser.password, salt, function(err, hash) {
        // Store hash in your password DB. 
        newUser.password=hash;

        if(err) throw err;
        newUser.save(callback);
    });
});

};


module.exports.findbyEmail= function(email,callback){
    const query ={email:email};
    User.findOne(query,callback);

};
 
module.exports.passwordCheck = function(PlainPassword,hash,callback){
 bcrypt.compare(PlainPassword, hash, function(err, res) {
   if(err) throw err;
   if(res){
       callback(null,res );
   }

});
};

module.exports.findUserbyId = function(id,callback){
    User.findOne(id,callback);

}


 
