const express= require('express');
const router = express.Router();
const User = require('./user');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const passport = require('passport');



router.post('/register',function(req,res){
      

         const newUser = new User({
             username:req.body.username,
             role:req.body.role,
             email:req.body.email,
             password:req.body.password
         });
        

         User.seveUser(newUser, function(err,user){
                    
             if(err){
                    res.json({status:false,msg:"data not inserted"});
             }
             if(user){
                 console.log(newUser); 
                    res.json({status:true,msg:"data inserted"});
             }
         });

});   

router.get("/user",function(req, res) {
     
User.find({},function(err, user){
            if(err) throw err;

            res.status(200).send({"success": true, "result": user});
    });
  }
 );



router.get("/assignusername/:username",function(req, res) {
     var username = req.params.username;
User.findOne({username:username},function(err, bms){
            if(err) 
            console.log(err);;

            res.status(200).send({"success": true, "result": bms});
    });
  }
 );

router.get("/role/:email",function(req, res) {
     var email = req.params.email;
User.findOne({email:email},function(err, user){
            if(err) 
            console.log(err);;

            res.status(200).send({"success": true, "result": user});
    });
  }
 );


router.put("/updateuser/:username", function (req, res) { 

    var user =""; 

    var username = req.params.username;

  // console.log(username);
    User.findOne({username:username},function(err, use){
            if(err) 
            console.log(err);

             if(use){
                 user =   use._id
                 console.log(user);
                  if ( user === "") 
                  {
          return res.json({ "success": false, "msg": "You need to send the ID of the diet plan" });
                }

          User.update({_id: user}, req.body, function (err) {
        if (err) {
            return res.json({ "success": false, "msg": "Error while updating User", "error": err });
        }
        res.status(200).json({ "success": true, "msg": " User Detail updated"  });
    });
            }

             });

   
});




router.post('/login',function(req,res){
 const email = req.body.email;
 const password = req.body.password;
 User.findbyEmail(email,function(err,user){

   if(err) throw err; 

    if(!user) 
    {
        res.json({status:false,msg:"no user found"});
        return false;
    }
    
    User.passwordCheck(password, user.password,function(err,match){
               if(err) throw err; 

               if(match){
                
                 const token = jwt.sign(user, config.secret,{expiresIn:86400});
                 
                 res.json(
                     {
                    status:true,
                    token:'JWT '+token,
                    user:{
                        id:user._id,
                        role:user.role,
                        username:user.username,
                        email:user.email
                         }
                      }
                         );

              }else if(!match)
               {
           res.json({status:false,msg:"password doest match"});
           return false;
               }

    });

 });
}); 






 
module.exports=router;