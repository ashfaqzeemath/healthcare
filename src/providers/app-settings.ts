import { Injectable } from '@angular/core';
 
const CONFIG = {
  apiUrl: 'http://127.0.0.1:3005/',
  //apiUrl: 'http://192.168.1.101:3005/',
};
 
@Injectable()
export class AppSettings {
 
  constructor() {
  }
 
  public getApiUrl() {
    return CONFIG.apiUrl;
  }
}