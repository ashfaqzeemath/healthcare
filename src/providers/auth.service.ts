import { Injectable } from '@angular/core';
import {Http ,Headers} from '@angular/http';
import 'rxjs/add/operator/map';





@Injectable()
export class AuthService {

  authToken: any;
  user: any;
  

 
  constructor(private http : Http ) {
  }

  registerUser(user)
  {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post("http://localhost:3005/register",user,{headers : headers}).map(res => res.json());
  }
  
  loginUser(user)
  {

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post("http://localhost:3005/login",user,{headers : headers}).map(res => res.json());
  
  }

/*
  getProfile()
  {
    this.fetchToken();

    let headers = new Headers();
    headers.append('Authorization', this.authToken);
   // headers.append('Content-Type','application/json');
    return this.http.get("http://localhost:3005/profile",{headers : headers}).map(res => res.json());
  }
  

  fetchToken()
  {
    const token = localStorage.getItem('tokenid');
    this.authToken = token;
  }
*/

  storeData(token,userdata)
  {
    localStorage.setItem('tokenid',token);
    localStorage.setItem('user',JSON.stringify(userdata));
    this.authToken =token;
    this.user = userdata;

  }

    storerole(email){
       return this.http.get('http://localhost:3005/role/'+email)
    .map(  
        res => res.json().result
    );
    }

   authusername()
   {
      return this.http.get('http://localhost:3005/user')
    .map(  
        res => res.json().result
    );
   } 


  assingnusername(username)
   {
      return this.http.get('http://localhost:3005/assignusername/'+username)
    .map(  
        res => res.json().result
    );
   } 
    updateusername( username,userdata)
   {
     console.log(username);
       console.log(userdata);

      return this.http.put('http://localhost:3005/updateuser/'+username ,userdata)
    .map(  
        res => res.json()
    );
   } 



  checklocalstorege()
  {
     const token = localStorage.getItem('tokenid');
      const user = localStorage.getItem('user');
      console.log(user);
    if(token == null && user == null)
    {
        return false;
    }
    else
    {
       return true;
    }


  }


  
}
 