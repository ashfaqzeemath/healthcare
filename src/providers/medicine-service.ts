import { AppSettings } from './app-settings';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MedicineService {

  apiUrl = this.appSettings.getApiUrl();
 
  constructor(public http: Http, public appSettings: AppSettings) {
  }
 
  public getMedicines() {
    return this.http.get(this.apiUrl + 'medicines')
      .map(response => response.json().result);
  }
 
  public addmedicine(newmedicine) {
    return this.http.post(this.apiUrl + 'medicine', newmedicine)
      .map(response => response.json());
  }

  public updatemedicine(medicineId, medicineBody) {
    return this.http.put(this.apiUrl + 'medicine/' + medicineId, medicineBody)
      .map(Response => Response.json());
  }
 
  public deletemedicine(medicineId) {
    return this.http.delete(this.apiUrl + 'medicine/' + medicineId)
      .map(response => response.json());
  }
}
