import { AppSettings } from './app-settings';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ForumService {
  apiUrl = this.appSettings.getApiUrl();
 
  constructor(public http: Http, public appSettings: AppSettings) {
  }
 
  public getForums() {
    return this.http.get(this.apiUrl + 'forum')
      .map(response => response.json().result);
  }

  public getForum(ForumId) {
    return this.http.get(this.apiUrl + 'forum/' + ForumId)
      .map(Response => Response.json().result);
  }
 
  public addForum(newForum) {
    return this.http.post(this.apiUrl + 'forum', newForum)
      .map(response => response.json());
  }

  public updateForum(ForumId, ForumBody) {
    return this.http.put(this.apiUrl + 'forum/' + ForumId, ForumBody)
      .map(Response => Response.json());
  }
 
  public deleteForum(ForumId) {
    console.log(ForumId);
    return this.http.delete(this.apiUrl + 'forum/' + ForumId)
      .map(response => response.json());
  }

  public manageComment(ForumId, CommentBody) {
    return this.http.put(this.apiUrl + 'forum/comment/' + ForumId, CommentBody)
      .map(Response => Response.json());
  }

  public deleteComment(ForumId, CommentId) {
    return this.http.put(this.apiUrl + 'forum/comment/' + ForumId + '/' + CommentId, null)
    .map(Response => Response.json());
  }
}
