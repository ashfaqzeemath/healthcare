import { Injectable } from '@angular/core';
import {Http ,Headers} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class dietService {







constructor(private http : Http ) {

}

dietInsert(diet)
{  
    return this.http.post('http://localhost:3005/savedietchart',diet)
    .map(res => res.json());
}

deletedietchart(diet)
{  
    return this.http.delete('http://localhost:3005/dietchat/'+diet,diet)
    .map(res => res.json());
}


 checkdietchartAvailability(bms)
{  
    return this.http.get('http://localhost:3005/checkbms/'+bms)
    .map(  
        res => res.json().result
    );
}

leadmeals(bms){
  console.log(bms);
     return this.http.get('http://localhost:3005/checkbms/'+bms)
    .map(  
        res => res.json().result
    );
}

 localstore(bms)
  {
      
    localStorage.setItem('bms' , JSON.stringify(bms));
  }
clearlocalstore()
  {
    localStorage.setItem('bms' , '');
  }
  getlocalstore()
  { 
    return localStorage.getItem('bms');
  }

bmsSearch()
{  
    return this.http.get('http://localhost:3005/searchdietchart')
    .map(  
        res => res.json().result
    );
}

 updatedietchart(bms)
{  
    return this.http.get('http://localhost:3005/assigndietchart/'+bms)
    .map(  
        res => res.json().result
    );
}

 updatedietchart1(bms,data)
{  
    console.log(data);
    return this.http.put('http://localhost:3005/updetdietchat/'+bms,data)
    .map(  
        res => res.json()
    );
}

}