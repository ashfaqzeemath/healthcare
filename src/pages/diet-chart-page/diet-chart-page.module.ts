import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DietChartPage } from './diet-chart-page';

@NgModule({
  declarations: [
    DietChartPage,
  ],
  imports: [
    IonicPageModule.forChild(DietChartPage),
  ],
  exports: [
    DietChartPage
  ]
})
export class DietChartPageModule {}
