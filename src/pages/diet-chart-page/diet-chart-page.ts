import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { DailyDietPage } from '../daily-diet-page/daily-diet-page';
import { WorkoutChartPage } from '../workout-chart-page/workout-chart-page';
import { Addietchart } from "../addietchart/addietchart";
import { DietChartUpdate } from "../diet-chart-update/diet-chart-update";
import { dietService } from "../../providers/diet.service";

/**
 * Generated class for the DietChartPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-diet-chart-page',
  templateUrl: 'diet-chart-page.html',
})
export class DietChartPage {
  hieght : number = 0;
  wieght : number = 0;
  bms : any = 0;
  Number : any;


 


  constructor(public navCtrl: NavController, public navParams: NavParams, private DietService : dietService  ) {
     this.isAdmin();
  }

  checkbms()
  {
   // this.DietService.clearlocalstore();
   return  this.bms =(this.wieght / (this.hieght * this.hieght));
  }

  bmsprediction()
  {
    if(this.bms < 18.5)
    {
      this.bms =18.5;
    }
    else if(this.bms> 18.6 && this.bms <24.9)
    {
      this.bms =22;
    }
    else if(this.bms> 25 && this.bms < 30)
    {
      this.bms =28;
    }
    else{
      this.bms =30;
    }
    console.log(this.bms );
    this.DietService.localstore(this.bms);

  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad DietChartPage');
  }

  pushDailyDietPage() {
      this.bmsprediction();
    this.navCtrl.push(DailyDietPage);
  }

  pushWorkoutChartPage(){
    this.navCtrl.push(WorkoutChartPage);
  }
  pushaddDietchartPage(){
        this.navCtrl.push(Addietchart);
  }
  pushupdatedietchartPage(){
        this.navCtrl.push(DietChartUpdate);
  }

    isAdmin(){

      this.Number=localStorage.getItem('role');
      if(this.Number == 1 ){
        return true;
      }

    }

  

}
