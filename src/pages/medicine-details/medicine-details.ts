import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';

import { MedicineService } from '../../providers/medicine-service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { MedicineSupporPage } from '../medicine-suppor-page/medicine-suppor-page';

@IonicPage()
@Component({
  selector: 'page-medicine-details',
  templateUrl: 'medicine-details.html',
})
export class MedicineDetails {

  medicine: Observable<any>;
  private edit_medicine_: FormGroup;
  private add_medicine_: FormGroup;
  add_form: boolean = false;
  edit_form: boolean = false;
  
  logged_username: String = '';
  logged_user_role: Number = 0; // 0 = Normal user, 1 = admin

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder, public medicineService: MedicineService,
    public toastCtrl: ToastController) {
    this.initializeMedicines();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicineDetails');
  }

  initializeMedicines() {
    this.medicine = this.navParams.get('p1');

    this.add_form = this.navParams.get('add');
    
    this.logged_username = this.navParams.get('username');
    this.logged_user_role = this.navParams.get('role');

    this.edit_medicine_ = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      created_by: [''],
    });

    this.add_medicine_ = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      created_by: [''],
    });
  }

  showAddForm() {
    this.add_form = true;
  }

  editForm() {
    this.edit_form = true;
  }

  saveMedicine(id) {
    this.medicineService.updatemedicine(id, this.edit_medicine_.value)
      .subscribe(data => {
        this.showToast(data.msg);
        this.resetForm();
        this.navCtrl.setRoot(MedicineSupporPage);
      },
      error => this.showToast(error.msg));
  }

  deleteMedicine(Id) {
    this.medicineService.deletemedicine(Id)
      .subscribe((data) => {
        this.showToast(data.msg);
        this.navCtrl.setRoot(MedicineSupporPage);
      },
      error => this.showToast(error.msg));
  }

  addMedicine() {
    this.add_medicine_.value.created_by = '1';
    this.medicineService.addmedicine(this.add_medicine_.value)
      .subscribe(res => {
        this.showToast(res.msg);
        this.resetForm();
        this.navCtrl.setRoot(MedicineSupporPage);
      },
      error => console.log(error.msg));
  }

  resetForm() {
    this.add_medicine_ = this.formBuilder.group({
      name: [''],
      description: [''],
      created_by: [''],
    });

    this.edit_medicine_ = this.formBuilder.group({
      name: [''],
      description: [''],
      created_by: [''],
    });
  }

  private showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

}
