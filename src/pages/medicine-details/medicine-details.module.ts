import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedicineDetails } from './medicine-details';

@NgModule({
  declarations: [
    MedicineDetails,
  ],
  imports: [
    IonicPageModule.forChild(MedicineDetails),
  ],
  exports: [
    MedicineDetails
  ]
})
export class MedicineDetailsModule {}
