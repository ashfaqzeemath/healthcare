import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LunchPage } from './lunch-page';

@NgModule({
  declarations: [
    LunchPage,
  ],
  imports: [
    IonicPageModule.forChild(LunchPage),
  ],
  exports: [
    LunchPage
  ]
})
export class LunchPageModule {}
