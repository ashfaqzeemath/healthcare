import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { dietService } from "../../providers/diet.service";

/**
 * Generated class for the LunchPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-lunch-page',
  templateUrl: 'lunch-page.html',
})
export class LunchPage {

  items: object;
  itembms: String;
  lunch1: String;
  lunch2: String;
  lunch3: String;
  lunch4: String;
  constructor(public navCtrl: NavController, public navParams: NavParams, private DietService: dietService) {
    this.initializeItems();
    this.itembms = localStorage.getItem('bms');
    if (this.itembms != null) {
      console.log(this.itembms);
      this.DietService.leadmeals(this.itembms).subscribe(res => {
        this.lunch1 = res.lunch1;
        this.lunch2 = res.lunch2;
        this.lunch3 = res.lunch3;
        this.lunch4 = res.lunch4;

      });

    } else {
      console.log('no value');
    }

  }

  initializeItems() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LunchPage');
  }

}
