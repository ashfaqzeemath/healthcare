import { ForumService } from '../../providers/forum-service';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ArticleDetailPage } from '../article-detail-page/article-detail-page';
import { AddForum } from '../add-forum/add-forum';

@IonicPage()
@Component({
  selector: 'page-forum-page',
  templateUrl: 'forum-page.html',
})
export class ForumPage {

  forums: Array<any>;
  public articleDetailspage: ArticleDetailPage

  logged_username: String = '';
  logged_user_role: Number = 0; // 0 = Normal user, 1 = admin

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public forumService: ForumService) {

    this.logged_username = localStorage.getItem('username');
    this.logged_user_role = Number(localStorage.getItem('role'));
    if ((localStorage.getItem('username') == null) && localStorage.getItem('role') == null) {
      this.logged_username = '';
      this.logged_user_role = 0;
    }
    this.loadForums();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForumPage');
  }

  openDetailsPage(forum) {
    this.navCtrl.push(ArticleDetailPage, { p1: forum, username: this.logged_username, role: this.logged_user_role, comment: false });
  }

  public loadForums() {
    this.forumService.getForums()
    .subscribe((data) => {
      this.forums = data;
    });

  }

  addForum() {
    this.navCtrl.push(AddForum, { username: this.logged_username, role: this.logged_user_role });
  }

  addComment(forum) {
    // const art = new ArticleDetailPage();
    this.navCtrl.push(ArticleDetailPage, { p1: forum, username: this.logged_username, role: this.logged_user_role, comment: true });
    // this.articleDetailspage.addComment();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.loadForums();
      refresher.complete();
    }, 2000);
  }
}
