import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';

import { ForumService } from '../../providers/forum-service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { ForumPage } from '../forum-page/forum-page';

@IonicPage()
@Component({
  selector: 'page-article-detail-page',
  templateUrl: 'article-detail-page.html',
})
export class ArticleDetailPage {
  forum: Array<any>;
  show_edit: boolean = false;
  edit_comment: boolean = false;
  private edit_forum_: FormGroup;
  private commet_form_: FormGroup;
  order: string = '-posted_on';

  logged_username: String = '';
  logged_user_role: Number = 0; // 0 = Normal user, 1 = admin

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public forumService: ForumService, public alertCtrl: AlertController,
    public toastCtrl: ToastController, private formBuilder: FormBuilder) {

    this.forum = this.navParams.get('p1');
    this.logged_username = this.navParams.get('username');
    this.logged_user_role = this.navParams.get('role');
    this.edit_comment = this.navParams.get('comment');

    console.log(this.edit_comment);

    console.log('this.logged_username: ', this.logged_username, '     this.logged_user_role; ', this.logged_user_role);
    this.edit_forum_ = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      sub_title_1: [''],
      sub_title_1_content: [''],
    });

    this.commet_form_ = this.formBuilder.group({
      comment_id: [''],
      posted_on: [''],
      author: [''],
      comment_text: ['', Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticleDetailPage');
  }

  editForum(forum) {
    this.show_edit = true;
  }

  deleteForum(id) {
    let confirm = this.alertCtrl.create({
      title: 'Caution?',
      message: 'Do you want delete this?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.forumService.deleteForum(id)
              .subscribe(data => {
                this.showToast(data.msg);
                this.navCtrl.setRoot(ForumPage);
                this.navCtrl.pop();
              },
              error => console.log(error.msg));
          }
        }
      ]
    });
    confirm.present();
  }

  addComment() {
    this.edit_comment = true;
  }

  saveForum(id) {
    this.forumService.updateForum(id, this.edit_forum_.value)
      .subscribe(data => {
        this.showToast(data.msg);
        this.resetForm();
        this.navCtrl.setRoot(ForumPage);
      },
      error => console.log(error.msg));
  }

  saveComment(forum_id, comnt_id, comment_posted_on) {

    this.commet_form_.value.comment_id = comnt_id;
    this.commet_form_.value.posted_on = comment_posted_on;
    this.commet_form_.value.author = this.logged_username;

    this.forumService.manageComment(forum_id, this.commet_form_.value)
      .subscribe(data => {
        this.showToast(data.msg);
        this.resetCommentForm();
        this.edit_comment = false;
        this.navCtrl.setRoot(ForumPage);
      },
      error => console.log(error.msg));
  }

  deleteComment(forum_id, comnt_id) {
    this.forumService.deleteComment(forum_id, comnt_id)
      .subscribe(data => {
        this.showToast(data.msg);
        this.navCtrl.setRoot(ForumPage);
      },
      error => console.log(error.msg));
  }

  private showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  resetForm() {
    this.edit_forum_ = this.formBuilder.group({
      title: [''],
      content: [''],
      sub_title_1: [''],
      sub_title_1_content: [''],
    });
  }

  resetCommentForm() {
    this.commet_form_ = this.formBuilder.group({
      comment_id: [''],
      posted_on: [''],
      author: [''],
      comment_text: [''],
    });
  }

  loadTheArticle(Fid) {
    this.forumService.getForum(Fid)
      .subscribe((data) => {

        this.forum = data;
      });
  }

  isAdmin() {
    console.log(this.logged_user_role);
    if (this.logged_user_role == 1) {
      return true;
    }
    return false;
  }

  isDoctor() {
    if (this.logged_user_role == 2) {
      return true;
    }
    return false;
  }

  showConfirm(id) {
    let confirm = this.alertCtrl.create({
      title: 'Caution?',
      message: 'Do you want delete this?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
}
