import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArticleDetailPage } from './article-detail-page';

@NgModule({
  declarations: [
    ArticleDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ArticleDetailPage),
  ],
  exports: [
    ArticleDetailPage
  ]
})
export class ArticleDetailPageModule {}
