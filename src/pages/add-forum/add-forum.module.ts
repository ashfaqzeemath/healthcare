import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddForum } from './add-forum';

@NgModule({
  declarations: [
    AddForum,
  ],
  imports: [
    IonicPageModule.forChild(AddForum),
  ],
  exports: [
    AddForum
  ]
})
export class AddForumModule {}
