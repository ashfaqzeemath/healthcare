import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ForumService } from './../../providers/forum-service';
import { ForumPage } from '../forum-page/forum-page';

import { LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-add-forum',
  templateUrl: 'add-forum.html',
})
export class AddForum {
  private new_forum_: FormGroup;
  logged_username: String = '';
  logged_user_role: Number = 0; // 0 = Normal user, 1 = admin

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private forumService: ForumService, private formBuilder: FormBuilder,
    public toastCtrl: ToastController, public loadingCtrl: LoadingController) {

    this.new_forum_ = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      sub_title_1: [''],
      sub_title_1_content: [''],
    });

    this.logged_username = this.navParams.get('username');
    this.logged_user_role = this.navParams.get('role');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddForum');
  }

  saveForum() {
    this.new_forum_.value.created_by = this.logged_username;
    this.forumService.addForum(this.new_forum_.value)
      .subscribe(res => {
        this.presentLoading();
        this.resetForm();
        this.navCtrl.setRoot(ForumPage);
      },
      error => this.showToast(error.msg));
  }

  private showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  resetForm() {
    this.new_forum_ = this.formBuilder.group({
      title: [''],
      content: [''],
      sub_title_1: [''],
      sub_title_1_content: [''],
    });
  }


  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }
}
