import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { dietService } from "../../providers/diet.service";
import {Observable} from "rxjs/Observable"

/**
 * Generated class for the DietChartUpdate page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-diet-chart-update',
  templateUrl: 'diet-chart-update.html',
})
export class DietChartUpdate {
    BMS: Array<any>;
    Updatedata:any;
    bmsrate:String;

  
    MorningMeal1:String;
    MorningMeal2:String;
    MorningMeal3:String;
    MorningMeal4:String;

    LunchMeal1:String;
    LunchMeal2:String;
    LunchMeal3:String;
    LunchMeal4:String;
    
    DinnerMeal1:String;
    DinnerMeal2:String;
    DinnerMeal3:String;
    DinnerMeal4:String;
   



  constructor(public navCtrl: NavController, public navParams: NavParams ,private DietService: dietService ,private toastCtrl: ToastController) {
    
     this.DietService.bmsSearch()
     .subscribe((data) => {
       this.BMS = data;
      //  this.BMS1 = data.bms;
       console.log(this.BMS);
     });
          
      }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DietChartUpdate');

  }
  searchDietChart(){
    console.log(this.bmsrate);
    this.DietService.updatedietchart(this.bmsrate)
     .subscribe((data) => {
       this.Updatedata = data;
       console.log(this.Updatedata.morning1);
    this.MorningMeal1=this.Updatedata.morning1;
    this.MorningMeal2=this.Updatedata.morning2;
    this.MorningMeal3=this.Updatedata.morning3;
    this.MorningMeal4=this.Updatedata.morning4;

    this.LunchMeal1=this.Updatedata.lunch1;
    this.LunchMeal2=this.Updatedata.lunch2;
    this.LunchMeal3=this.Updatedata.lunch3;
    this.LunchMeal4=this.Updatedata.lunch4;

    this.DinnerMeal1=this.Updatedata.dinner1;
    this.DinnerMeal2=this.Updatedata.dinner2;
    this.DinnerMeal3=this.Updatedata.dinner3;
    this.DinnerMeal4=this.Updatedata.dinner4;
     });
     


  }

  UpdateDietChart(){

   if(this.validatetextbox()==false){

   const diet = {
          bms: this.bmsrate,
          morningmeal1:this.MorningMeal1,
          morningmeal2:this.MorningMeal2,
          morningmeal3:this.MorningMeal3,
          morningmeal4:this.MorningMeal4,


          lunchmeal1:this.LunchMeal1,
          lunchmeal2:this.LunchMeal2,
          lunchmeal3:this.LunchMeal3,
          lunchmeal4:this.LunchMeal4,


          dinnermeal1:this.DinnerMeal1,
          dinnermeal2:this.DinnerMeal2,
          dinnermeal3:this.DinnerMeal3,
          dinnermeal4:this.DinnerMeal4
      };
  
    

       this.DietService.updatedietchart1(this.bmsrate,diet).subscribe((data) => {
                         console.log(data);
          });
              let toast = this.toastCtrl.create({
                    message: 'Diet Chart Updated',
                    duration: 3000
                    });
                    toast.present();
           }
        else{
          let toast = this.toastCtrl.create({
                    message: 'Diet Chart Updated',
                    duration: 3000
                    });
                    toast.present();
        }  
        }

           DeleteDietChart()
          {
                this.DietService.deletedietchart(this.bmsrate).subscribe((data) => {
                         console.log(data);
          });
             let toast = this.toastCtrl.create({
                    message: 'You are not inserted a field ',
                    duration: 3000
                    });
                    toast.present();

          }  


          validatetextbox(){
   if((this.MorningMeal1 == null || this.MorningMeal1 =="")) {
    return true;
  }
   else if((this.MorningMeal2 == null || this.MorningMeal2 =="")) {
    return true;
  }
   else if((this.MorningMeal3 == null || this.MorningMeal3 =="")) {
    return true;
  }
   else if((this.MorningMeal4 == null || this.MorningMeal4 =="")) {
    return true;
  }
   else if((this.LunchMeal1 == null || this.LunchMeal1 =="")) {
    return true;
  }
   else if((this.LunchMeal2 == null || this.LunchMeal2 =="")) {
    return true;
  }
   else if((this.LunchMeal3 == null || this.LunchMeal3 =="")) {
    return true;
  }
   else if((this.LunchMeal4 == null || this.LunchMeal4 =="")) {
    return true;
  }
     else if((this.DinnerMeal1 == null || this.DinnerMeal1 =="")) {
    return true;
  }
   else if((this.DinnerMeal2 == null || this.DinnerMeal2 =="")) {
    return true;
  }
   else if((this.DinnerMeal3 == null || this.DinnerMeal3 =="")) {
    return true;
  }
   else if((this.DinnerMeal3 == null || this.DinnerMeal3 =="")) {
    return true;
    }
}

}
