import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';
import { DietChartUpdate } from './diet-chart-update';

@NgModule({
  declarations: [
    DietChartUpdate,
  ],
  imports: [
    IonicPageModule.forChild(DietChartUpdate),
  ],
  exports: [
    DietChartUpdate
  ]
})
export class DietChartUpdateModule {}
