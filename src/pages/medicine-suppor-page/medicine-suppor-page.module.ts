import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedicineSupporPage } from './medicine-suppor-page';

@NgModule({
  declarations: [
    MedicineSupporPage,
  ],
  imports: [
    IonicPageModule.forChild(MedicineSupporPage),
  ],
  exports: [
    MedicineSupporPage
  ]
})
export class MedicineSupporPageModule {}
