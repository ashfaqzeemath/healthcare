import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MedicineDetails } from '../medicine-details/medicine-details';

import { MedicineService } from '../../providers/medicine-service';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-medicine-suppor-page',
  templateUrl: 'medicine-suppor-page.html',
})
export class MedicineSupporPage {

  searchQuery: string;
  medicines: Array<any> = [];
  temp_arr: Array<any> = [];
  all_medicines: Observable<any>;

  logged_username: String = '';
  logged_user_role: Number = 1; // 0 = Normal user, 1 = admin
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public medicineService: MedicineService) {
    this.initializeMedicines();

    this.logged_username = localStorage.getItem('username');
    this.logged_user_role = Number(localStorage.getItem('role'));
    if ((localStorage.getItem('username') == null) && localStorage.getItem('role') == null) {
      this.logged_username = '';
      this.logged_user_role = 0;
    }
  }

  initializeMedicines() {

    this.loadMedicines();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicineSupporPage');
  }

  loadMedicines() {
    this.medicineService.getMedicines()
      .subscribe((data) => {
        this.medicines = data;
        //console.log(this.medicines.forEach(function(e){console.log(e.name)}));
      });
  }

  openDetailPage(medicine) {
    this.navCtrl.push(MedicineDetails, { p1: medicine, add: false, username: this.logged_username, role: this.logged_user_role });
  }

  addMedicine() {
    this.temp_arr = [{ name: '', description: '' }]
    this.navCtrl.push(MedicineDetails, { p1: this.temp_arr, add: true, username: this.logged_username, role: this.logged_user_role });
  }

  isAdmin() {
    if (this.logged_user_role == 1) {
      return true;
    }
    return false;
  }

  isDoctor() {
    if (this.logged_user_role == 2) {
      return true;
    }
    return false;
  }
}
