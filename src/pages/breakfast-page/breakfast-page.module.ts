import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BreakfastPage } from './breakfast-page';

@NgModule({
  declarations: [
    BreakfastPage,
  ],
  imports: [
    IonicPageModule.forChild(BreakfastPage),
  ],
  exports: [
    BreakfastPage
  ]
})
export class BreakfastPageModule {}
