import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { DietChartPage } from "../diet-chart-page/diet-chart-page";
import { dietService } from "../../providers/diet.service";
import { DietChartPage } from "../diet-chart-page/diet-chart-page";

/**
 * Generated class for the BreakfastPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-breakfast-page',
  templateUrl: 'breakfast-page.html',
})
export class BreakfastPage {

  items: Array<any>;
  itembms:String;
  morning1:String;
  morning2:String;
  morning3:String;
  morning4:String;

  constructor(public navCtrl: NavController, public navParams: NavParams ,private DietService : dietService  ) 
  {
    this.itembms = localStorage.getItem('bms');
    
   if(this.itembms != null)
   {
    console.log(this.itembms);
   this.DietService.leadmeals(this.itembms).subscribe(res => {
   this.morning1 = res.morning1;
   this.morning2 = res.morning2;
   this.morning3 = res.morning3;
   this.morning4 = res.morning4;
  });

   }else{
   console.log('no value');
  }
}

/*
  initializeItems() {
    this.items = ['Scrambled omelette toast topper',
                  'One-pan summer eggs',
                  'Flash-fried smoked salmon & egg bagel',
                  'Ultimate makeover full English',
                  'Full English frittata'];
  }*/

  ionViewDidLoad() {
    console.log('ionViewDidLoad BreakfastPage');
  }



}
