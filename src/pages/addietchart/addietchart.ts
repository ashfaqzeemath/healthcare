import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { dietService } from "../../providers/diet.service";


/**
 * Generated class for the Addietchart page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-addietchart',
  templateUrl: 'addietchart.html',
})
export class Addietchart {

BMS:String;
MorningMeal1:String;
MorningMeal2:String;
MorningMeal3:String;
MorningMeal4:String;
LunchMeal1:String;
LunchMeal2:String;
LunchMeal3:String;
LunchMeal4:String;
DinnerMeal1:String;
DinnerMeal2:String;
DinnerMeal3:String;
DinnerMeal4:String;



  constructor(public navCtrl: NavController, public navParams: NavParams ,private DietService:dietService,private toastCtrl: ToastController) {
  }

     
  ionViewDidLoad() {
    console.log('ionViewDidLoad Addietchart');
  }

  addDietChart(){

    if(this.validatetextbox()){


      const diet = {
          bms:this.BMS,
          morningmeal1:this.MorningMeal1,
          morningmeal2:this.MorningMeal2,
          morningmeal3:this.MorningMeal3,
          morningmeal4:this.MorningMeal4,
          lunchmeal1:this.LunchMeal1,
          lunchmeal2:this.LunchMeal2,
          lunchmeal3:this.LunchMeal3,
          lunchmeal4:this.LunchMeal4,
          dinnermeal1:this.DinnerMeal1,
          dinnermeal2:this.DinnerMeal2,
          dinnermeal3:this.DinnerMeal3,
          dinnermeal4:this.DinnerMeal4
      };
      let toast = this.toastCtrl.create({
                    message: 'Diet Chart Added',
                    duration: 3000
                    });
                    toast.present();
      console.log(diet)
      this.DietService.dietInsert(diet).subscribe(
        res => { console.log(res);  
    },
    error => console.log(error));
    }
    else
    {
         let toast = this.toastCtrl.create({
                    message: 'You are not inserted a field ',
                    duration: 3000
                    });
                    toast.present();
    }
}

validatetextbox(){
 // if((this.BMS == null || this.BMS =="")){
    return true;
   // }
  /*else if((this.MorningMeal1 == null || this.MorningMeal1 =="")) {
    return true;
  }
   else if((this.MorningMeal2 == null || this.MorningMeal2 =="")) {
    return true;
  }
   else if((this.MorningMeal3 == null || this.MorningMeal3 =="")) {
    return true;
  }
   else if((this.MorningMeal4 == null || this.MorningMeal4 =="")) {
    return true;
  }
   else if((this.LunchMeal1 == null || this.LunchMeal1 =="")) {
    return true;
  }
   else if((this.LunchMeal2 == null || this.LunchMeal2 =="")) {
    return true;
  }
   else if((this.LunchMeal3 == null || this.LunchMeal3 =="")) {
    return true;
  }
   else if((this.LunchMeal4 == null || this.LunchMeal4 =="")) {
    return true;
  }
     else if((this.DinnerMeal1 == null || this.DinnerMeal1 =="")) {
    return true;
  }
   else if((this.DinnerMeal2 == null || this.DinnerMeal2 =="")) {
    return true;
  }
   else if((this.DinnerMeal3 == null || this.DinnerMeal3 =="")) {
    return true;
  }
   else if((this.DinnerMeal3 == null || this.DinnerMeal3 =="")) {
    return true;
}*/
}

}
