import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Addietchart } from './addietchart';

@NgModule({
  declarations: [
    Addietchart,
  ],
  imports: [
    IonicPageModule.forChild(Addietchart),
  ],
  exports: [
    Addietchart
  ]
})
export class AddietchartModule {}
