import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController,  App } from 'ionic-angular';
import { AuthService } from "../../providers/auth.service";
import { ForumPage } from "../forum-page/forum-page";
import { SignUpPage } from "../sign-up-page/sign-up-page";

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login-page',
  templateUrl: 'login-page.html',
})
export class LoginPage {

  email : String;
  password : String;

  constructor(public navCtrl: NavController, public navParams: NavParams , private authService : AuthService ,private toastCtrl: ToastController,public  appCtrl: App,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  loginUser(){
    const User={
      email:this.email,
      password:this.password
    };

    this.authService.loginUser(User).subscribe(res =>{
     

          if(res.status){

                  this.authService.storeData(res.token,res.user);
                  this.authService.storerole(User.email).subscribe((res)=> {
                    localStorage.setItem('role',res.role);
                     localStorage.setItem('username',res.username);
                  }) ;
                    let toast = this.toastCtrl.create({
                    message: 'User loged successfully',
                    duration: 3000
                    });
                    toast.present();
      
                  this.navCtrl.push( ForumPage);
              }
              else{
                    let toast = this.toastCtrl.create({
                    message: 'User not loged successfully',
                    duration: 3000
                    });
                    toast.present();
              }


      });
  }

  pushRegister(){

 this.navCtrl.push(SignUpPage);

  }




}
