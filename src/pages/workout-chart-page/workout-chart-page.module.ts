import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkoutChartPage } from './workout-chart-page';

@NgModule({
  declarations: [
    WorkoutChartPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkoutChartPage),
  ],
  exports: [
    WorkoutChartPage
  ]
})
export class WorkoutChartPageModule {}
