import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from "../../providers/auth.service";

/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class Profile {
  

  username :Array<any>;
  user:String;
  UserName:String;
  Role:String;
  Email:String;
  password:String;
  __v:String;
  
  

  constructor(public navCtrl: NavController, public navParams: NavParams ,private authService :AuthService) {

 this.authService.authusername()
     .subscribe((data) => {
       this.username = data;
      //  this.BMS1 = data.bms;
       console.log(this.username);
     });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Profile');
  }

    UpdateUser()
     {  
      const  userdetail  = {
            username: this.UserName,
            role: this.Role,
            email: this.Email,
            password:this.password,
            __v: this.__v
      };

      console.log(userdetail);
          console.log(this.user);
      this.authService.updateusername(this.user,userdetail).subscribe((data)=>{

               console.log(data);
      });
    }

    setuserdetail(){
        this.authService.assingnusername(this.user).subscribe((data) => {
       this.UserName = data.username;
       this.Role = data.role;
       this.Email = data.email;
        this.password =data.password;
        this.__v= data.__v;

    
    
     });


    }







}
