import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DinnerPage } from './dinner-page';

@NgModule({
  declarations: [
    DinnerPage,
  ],
  imports: [
    IonicPageModule.forChild(DinnerPage),
  ],
  exports: [
    DinnerPage
  ]
})
export class DinnerPageModule {}
