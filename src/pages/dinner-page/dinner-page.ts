import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { dietService } from "../../providers/diet.service";

/**
 * Generated class for the DinnerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-dinner-page',
  templateUrl: 'dinner-page.html',
})
export class DinnerPage {


  items: Array<any>;
  itembms: String = '';
  dinner1: String = '';
  dinner2: String = '';
  dinner3: String = '';
  dinner4: String = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private DietService: dietService) {
    this.initializeItems();
    this.itembms = localStorage.getItem('bms');

    if (this.itembms != null) {
      console.log(this.itembms);
      this.DietService.leadmeals(this.itembms).subscribe(res => {
        this.dinner1 = res.dinner1;
        this.dinner2 = res.dinner2;
        this.dinner3 = res.dinner3;
        this.dinner4 = res.dinner4;


        console.log(this.items);
      });

    } else {
      console.log('no value');
    }


  }

  initializeItems() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DinnerPage');
  }

}
