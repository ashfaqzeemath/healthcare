import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams ,ToastController,ViewController} from 'ionic-angular';
import { AuthService } from "../../providers/auth.service";
//import { LoginPage } from "../login-page/login-page";
import { ForumPage } from "../forum-page/forum-page";

/**
 * Generated class for the SignUpPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-sign-up-page',
  templateUrl: 'sign-up-page.html',
})
export class SignUpPage {

  username:String;
  name:String;
  email:String;
  password:String;



  constructor(public navCtrl: NavController, public navParams: NavParams, private authService : AuthService,private toastCtrl: ToastController,public  appCtrl: App,public viewCtrl: ViewController ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }



regesterdata(){
     const user={
        username:this.username,
        role:'0',
        email:this.email,
        password:this.password
          };
     
          

  this.authService.registerUser(user).subscribe(res => {
      console.log(res);

    if(res.status){
          let toast = this.toastCtrl.create({
          message: 'User was added successfully',
          duration: 3000
          });
          toast.present();
         
        this.navCtrl.push(ForumPage);
    }
    else{
          let toast = this.toastCtrl.create({
          message: 'User was not added successfully',
          duration: 3000
          });
          toast.present();
    }

      
  });


}






}
