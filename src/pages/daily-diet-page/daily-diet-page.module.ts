import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyDietPage } from './daily-diet-page';

@NgModule({
  declarations: [
    DailyDietPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyDietPage),
  ],
  exports: [
    DailyDietPage
  ]
})
export class DailyDietPageModule {}
