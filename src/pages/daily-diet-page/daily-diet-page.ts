import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BreakfastPage } from '../breakfast-page/breakfast-page';
import { LunchPage } from '../lunch-page/lunch-page';
import { DinnerPage } from '../dinner-page/dinner-page';
import { AuthService } from "../../providers/auth.service";
import { DietChartPage } from "../diet-chart-page/diet-chart-page";

/**
 * Generated class for the DailyDietPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-daily-diet-page',
  templateUrl: 'daily-diet-page.html',
})
export class DailyDietPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private authService :AuthService ) {

  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad DailyDietPage');
  
    
  }

  pushBreakfastPage() {
    this.navCtrl.push(BreakfastPage);
  }

  pushLunchPage() {
    this.navCtrl.push(LunchPage);
  }

  pushDinnerPage() {
    this.navCtrl.push(DinnerPage);
  }

}
