import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { AppSettings } from '../providers/app-settings';
import { ForumService } from '../providers/forum-service';
import { MedicineService } from '../providers/medicine-service';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { ForumPage } from '../pages/forum-page/forum-page';
import { DietChartPage } from '../pages/diet-chart-page/diet-chart-page';
import { DailyDietPage } from '../pages/daily-diet-page/daily-diet-page';
import { WorkoutChartPage } from '../pages/workout-chart-page/workout-chart-page';
import { MedicineSupporPage } from '../pages/medicine-suppor-page/medicine-suppor-page';
import { ArticleDetailPage } from '../pages/article-detail-page/article-detail-page';
import { SignUpPage } from '../pages/sign-up-page/sign-up-page';
import { LoginPage } from '../pages/login-page/login-page';
import { BreakfastPage } from '../pages/breakfast-page/breakfast-page';
import { LunchPage } from '../pages/lunch-page/lunch-page';
import { DinnerPage } from '../pages/dinner-page/dinner-page';
import { AddForum } from '../pages/add-forum/add-forum';
import { MedicineDetails } from '../pages/medicine-details/medicine-details';

import {Autosize} from 'ionic2-autosize';
import { ReversePipe } from './pipes/ReversePipe';
import { FilterPipe } from './pipes/FilterPipe';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AuthService } from "../providers/auth.service";
import { Addietchart } from "../pages/addietchart/addietchart";
import { dietService } from "../providers/diet.service";
import { DietChartUpdate } from "../pages/diet-chart-update/diet-chart-update";
import { Logout } from "../pages/logout/logout";
import { Profile } from "../pages/profile/profile";

@NgModule({
  declarations: [
    MyApp,
    ForumPage,
    DietChartPage,
    DailyDietPage,
    WorkoutChartPage,
    MedicineSupporPage,
    ArticleDetailPage,
    SignUpPage,
    LoginPage,
    BreakfastPage,
    LunchPage,
    DinnerPage,
    AddForum,
    MedicineDetails,
    Addietchart,
    Autosize,
    ReversePipe,
    FilterPipe,
    DietChartUpdate,
    Logout,
    Profile
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ForumPage,
    DietChartPage,
    DailyDietPage,
    WorkoutChartPage,
    MedicineSupporPage,
    ArticleDetailPage,
    SignUpPage,
    LoginPage,
    BreakfastPage,
    LunchPage,
    DinnerPage,
    AddForum,
    MedicineDetails,
    Addietchart,
    DietChartUpdate,
    Logout,
    Profile
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AppSettings,
    ForumService,
    MedicineService,
    dietService,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
