import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ForumPage } from '../pages/forum-page/forum-page';
import { DietChartPage } from '../pages/diet-chart-page/diet-chart-page';
import { MedicineSupporPage } from '../pages/medicine-suppor-page/medicine-suppor-page';
import { SignUpPage } from '../pages/sign-up-page/sign-up-page';
import { LoginPage } from '../pages/login-page/login-page';
import { Logout } from "../pages/logout/logout";
import { Profile } from "../pages/profile/profile";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = ForumPage;
  tokenId: any = localStorage.getItem('tokenid');

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();


    // used for an example of ngFor and navigation

    if (this.tokenId == null) {
      this.pages = [
        { title: 'Login', component: LoginPage }

      ];

    } else {
      this.pages = [
        { title: 'Forum', component: ForumPage },
        { title: 'Diet Chart', component: DietChartPage },
        { title: 'Medicine Support', component: MedicineSupporPage },
        { title: 'Register', component: SignUpPage },
        { title: 'LogOut', component: Logout },
        { title: 'Profile', component: Profile }
      ];

    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
